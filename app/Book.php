<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    protected $fillable = [
        'isbn',
        'title',
        'category',
        'publisher',
        'author_id',
    ];

    public function author()
    {
        return $this->belongsTo(Author::class);
    }
}
