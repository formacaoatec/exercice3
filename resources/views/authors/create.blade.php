@extends('shared.base')

@section('content')
<h1>Criar novo autor</h1>
<form action="/authors" method="post">
    @include('authors.form')
    <button type="submit" class="btn btn-primary">Criar</button>
</form>
@endsection