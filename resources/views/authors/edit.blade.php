@extends('shared.base')

@section('content')
<h1>Editar livro #{{ $author->id }}</h1>
<form action="/authors/{{ $author->id }}" method="post">
    @method('put')
    @include('authors.form')
    <button type="submit" class="btn btn-primary">Guardar</button>
</form>
@endsection