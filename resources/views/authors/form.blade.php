@csrf
<div class="form-group">
    <label for="name">Nome</label>
    <input type="text" name="name" class="form-control" placeholder="Nome" value="{{ $author->name }}">
</div>

<div class="form-group">
    <label for="nationality">Nacionalidade</label>
    <input type="text" name="nationality" class="form-control" placeholder="Nacionalidade" value="{{ $author->nationality }}">
</div>

