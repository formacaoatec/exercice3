@extends('shared.base')

@section('content')
<h1>Listagem de Autores</h1>
<p>
    <a href="/authors/create" class="btn btn-primary">Novo</a>
</p>
<table class="table">
    <thead>
        <tr>
            <th scope="col">Nome</th>
            <th scope="col">Nacionalidade</th>
            <th scope="col">Ações</th>
        </tr>
    </thead>
    <tbody>
        @foreach($authors as $author)
        <tr>
            <td><a href="/authors/{{ $author->id }}">{{ $author->name }}</a></td>
            <td>{{ $author->nationality }}</td>
            <td>
                <form action="/authors/{{ $author->id }}" method="post">
                    <a href="/authors/{{ $author->id }}/edit" class="btn btn-success">Editar</a>
                    @method("DELETE")
                    @csrf
                    <input type="submit" class="btn btn-danger" value="Eliminar">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection