@extends('shared.base')

@section('content')
    <h1>Detalhes do Autor #{{ $author->id }}</h1>
    <p>
        <form action="/authors/{{ $author->id }}" method="post">
            <a href="/authors/{{ $author->id }}/edit" class="btn btn-success">Editar</a>
            @method("DELETE")
            @csrf
            <input type="submit" class="btn btn-danger" value="Eliminar">
        </form>
    </p>
    <table class="table">
        <tbody>
            <tr>
                <th scope="row">Nome</th>
                <td>{{ $author->name }}</td>
            </tr>
            <tr>
                <th scope="row">Nationalidade</th>
                <td>{{ $author->nationality }}</td>
            </tr>
        </tbody>
        </table>
@endsection