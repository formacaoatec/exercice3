@extends('shared.base')

@section('content')
<h1>Criar novo livro</h1>
<form action="/books" method="post">
    @include('books.form')
    <button type="submit" class="btn btn-primary">Criar</button>
</form>
@endsection