@extends('shared.base')

@section('content')
<h1>Editar livro #{{ $book->isbn }}</h1>
<form action="/books/{{ $book->id }}" method="post">
    @method('put')
    @include('books.form')
    <button type="submit" class="btn btn-primary">Guardar</button>
</form>
@endsection