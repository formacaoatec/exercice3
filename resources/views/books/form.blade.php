@csrf
<div class="form-group">
    <label for="isbn">ISBN</label>
    <input type="text" name="isbn" class="form-control" placeholder="ISBN" value="{{ $book->isbn }}">
</div>

<div class="form-group">
    <label for="title">Titulo</label>
    <input type="text" name="title" class="form-control" placeholder="Titulo" value="{{ $book->title }}">
</div>

<div class="form-group">
    <label for="category">Categoria</label>
    <input type="text" name="category" class="form-control" placeholder="Categoria" value="{{ $book->category }}">
</div>

<div class="form-group">
    <label for="publisher">Editora</label>
    <input type="text" name="publisher" class="form-control" placeholder="Editora" value="{{ $book->publisher }}">
</div>

<div class="form-group">
    <label for="author_id">Autor</label>
    <select class="form-control" name="author_id">
    @foreach($authors as $author)
        <option value="{{ $author->id }}" {{ $author->id == $book->author_id ? 'selected' : '' }}>{{ $author->name }}</option>
    @endforeach
    </select>
</div>
