@extends('shared.base')

@section('content')
<h1>Listagem de Livros</h1>
<p>
    <a href="/books/create" class="btn btn-primary">Novo</a>
</p>
<table class="table">
    <thead>
        <tr>
            <th scope="col">ISBN</th>
            <th scope="col">Title</th>
            <th scope="col">Category</th>
            <th scope="col">Author</th>
            <th scope="col">Ações</th>
        </tr>
    </thead>
    <tbody>
        @foreach($books as $book)
        <tr>
            <td><a href="/books/{{ $book->id }}">{{ $book->isbn }}</a></td>
            <td><a href="/books/{{ $book->id }}">{{ $book->title }}</a></td>
            <td>{{ $book->category }}</td>
            <td><a href="/authors/{{ $book->author->id }}">{{ $book->author->name }}</a></td>
            <td>
                <form action="/books/{{ $book->id }}" method="post">
                    <a href="/books/{{ $book->id }}/edit" class="btn btn-success">Editar</a>
                    @method("DELETE")
                    @csrf
                    <input type="submit" class="btn btn-danger" value="Eliminar">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection