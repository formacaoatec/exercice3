@extends('shared.base')

@section('content')
    <h1>Detalhes do Livro #{{ $book->isbn }}</h1>
    <p>
        <form action="/books/{{ $book->id }}" method="post">
            <a href="/books/{{ $book->id }}/edit" class="btn btn-success">Editar</a>
            @method("DELETE")
            @csrf
            <input type="submit" class="btn btn-danger" value="Eliminar">
        </form>
    </p>
    <table class="table">
        <tbody>
            <tr>
                <th scope="row">ISBN</th>
                <td>{{ $book->isbn }}</td>
            </tr>
            <tr>
                <th scope="row">Titulo</th>
                <td>{{ $book->title }}</td>
            </tr>
            <tr>
                <th scope="row">Categoria</th>
                <td>{{ $book->category }}</td>
            </tr>
            <tr>
                <th scope="row">Editora</th>
                <td>{{ $book->publisher }}</td>
            </tr>
            <tr>
                <th scope="row">Autor</th>
                <td><a href="/authors/{{ $book->author->id }}">{{ $book->author->name }}</a></td>
            </tr>
        </tbody>
        </table>
@endsection