<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/app.css">
    <style>
        body { background-color: #fff; }
    </style>
    <title>Exercício 3</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Exercício 3</a>
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="/">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/books">Livros</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/authors">Autores</a>
      </li>
    </ul>
</nav>
<div class="row">
    <div class="col-md-12">
        <div class="container">
            @yield('content')
        </div>        
    </div>
</div>
</body>
</html>